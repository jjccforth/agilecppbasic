/*
 * C++ Application for Ajile PS development.
 *
 * Purpose:
 * A extensible command line tool for testing different functions.
 *
 * Usage:
 * When a PS function needs to be tested, write a test function and attach
 * it to a command
 *
 *
 */
#include <stdio.h>
#include <stdlib.h>

#include "platform.h"
#include "xparameters.h"
#include "xqspips.h"		/* QSPI device driver */

#include "xil_types.h"
#include "xuartps_hw.h"

#include "Xscugic.h"
#include "Xil_exception.h"
#include "xscutimer.h"

#include "SerialCommand.h"
#include "qspi_int_util.h"

#include "command.h"

#include "data_transfer.h"
/************************** Constant Definitions *****************************/

//timer info
#define TIMER_DEVICE_ID		XPAR_XSCUTIMER_0_DEVICE_ID
#define INTC_DEVICE_ID		XPAR_SCUGIC_SINGLE_DEVICE_ID
#define TIMER_IRPT_INTR		XPAR_SCUTIMER_INTR

#define TIMER_LOAD_VALUE	0x13DE4355




/************************** Global Variable *********************************/
SerialCommand sCmd;


u32 FlashReadBaseAddress = 0;
//static XScuGic Intc; //GIC
XScuTimer Timer;//timer


/****For read flash****/
extern XScuGic IntcInstance;
extern XQspiPs QspiInstance;

static void SetupInterruptSystem(XScuGic *GicInstancePtr,
		XScuTimer *TimerInstancePtr, u16 TimerIntrId);

static void TimerIntrHandler(void *CallBackRef);

u32  TimerFlag;

#define VERSION "1.1"

extern bool StartTimer;

void RecieveFileEx();
int WriteFlashEx(u8* Buffer,u32 OffsetSec,u32 LengthInPage);
int main()
{
	int Status;

	XScuTimer_Config *TMRConfigPtr;     //timer config
	 init_platform();

    //timer initialisation
    TMRConfigPtr = XScuTimer_LookupConfig(TIMER_DEVICE_ID);
    XScuTimer_CfgInitialize(&Timer, TMRConfigPtr,TMRConfigPtr->BaseAddr);
    XScuTimer_SelfTest(&Timer);
	 //load the timer
	 XScuTimer_LoadTimer(&Timer, TIMER_LOAD_VALUE);
	 //XScuTimer_EnableAutoReload(&Timer);
	 //start timer
	 XScuTimer_Start(&Timer);



	//Init QSPI flash
	Status = QspiInit(&IntcInstance, &QspiInstance, QSPI_DEVICE_ID, QSPI_INTR_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("InitQspi failed \n\r");
		return XST_FAILURE;
	}


    SetupInterruptSystem(&IntcInstance, &Timer,TIMER_IRPT_INTR);

	GPIOInit();

	//init_platform();
	//Insert a long press check
	//LongPress();
	//Print hint
	xil_printf("Ajile basic boot utility, Version %s\n", VERSION);
	sCmd.addCommand("DF",DumpFlash);
	sCmd.addCommand("FRID",ReadFlashId);
	sCmd.addCommand("UF",WritePackets);
	sCmd.addCommand("LP", LongPress);
	sCmd.addCommand("BL", ToggleLed);
	sCmd.addCommand("EF",EraseFlashCmd);
	sCmd.addCommand("BLMORE",BlinkLedFewTimes);
	sCmd.addCommand("UFX",RecieveFileEx);
	sCmd.setDefaultHandler(unrecognized);
    while(1)
    {
    	sCmd.readSerial();
    	if(StartTimer == true){
    		xil_printf("StartTimer tiggered\n");

    		//load timer
    		XScuTimer_LoadTimer(&Timer, TIMER_LOAD_VALUE);
    		//start timer
    		XScuTimer_Start(&Timer);
    		StartTimer = false;
    	}
    }
	return 0;
}

static void SetupInterruptSystem(XScuGic *GicInstancePtr,XScuTimer *TimerInstancePtr, u16 TimerIntrId)
{
		Xil_ExceptionInit();
//		Should not call a XScuGic_CfgInitialize when there's already an initialization
	    //connect to the hardware
		Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
					(Xil_ExceptionHandler)XScuGic_InterruptHandler,
					GicInstancePtr);

		//set up the timer interrupt
		XScuGic_Connect(GicInstancePtr, TimerIntrId,
						(Xil_ExceptionHandler)TimerIntrHandler,
						(void *)TimerInstancePtr);

		//enable the interrupt for the Timer at GIC
		XScuGic_Enable(GicInstancePtr, TimerIntrId);
		//enable interrupt on the timer
		XScuTimer_EnableInterrupt(TimerInstancePtr);
		// Enable interrupts in the Processor.
		Xil_ExceptionEnableMask(XIL_EXCEPTION_IRQ);
	}


static void TimerIntrHandler(void *CallBackRef)
{

	XScuTimer *TimerInstancePtr = (XScuTimer *) CallBackRef;
	XScuTimer_ClearInterruptStatus(TimerInstancePtr);
	xil_printf("*TimerISR*\n\r");
	ClearTimerFlag();
}


void SetTimerFlag(u32 mask)
{
	TimerFlag |= mask;
}

void ClearTimerFlag()
{
	TimerFlag = 0;
}


#define PARAMETER_OFFSET	0xF80000
#define TIMER_1MICROSECOND	0x14D
#define PACKET_OVERHEAD 7
#define IN_BUFFER_SIZE 0x60000
u8 data[IN_BUFFER_SIZE];


void RecieveFileEx(){
    u8 *InputStrPtr = data;
    u32 Count = 0;
    u32 Size = 1000;
    char Result;
    u32 LengthInPage = 0;
    u32 OffsetInSector = 0;

	char *to;
	unsigned long lto;
	to = sCmd.next();
	if (to == NULL)
		lto = 100; //100 microsnd as default
	else
		lto = strtol (to,NULL,10);
	printf("timeout value %lu\n",lto);



	//set a time out value for timer
	//load timer
	XScuTimer_LoadTimer(&Timer, TIMER_1MICROSECOND*lto );
	//start timer
	XScuTimer_Start(&Timer);
	SetTimerFlag(UART_READBYTE_FLAG);
	while(1)
    {


 	  InputStrPtr = data + Count;
	   *InputStrPtr = XUartPs_RecvByteEx(STDOUT_BASEADDRESS, 500, &Result);

	   if(Result != 0) {

		   print("Timeout while transferring\n");
		   break;

	   }
	   //A byte arrived. Reload timeout value for next one
	   XScuTimer_LoadTimer(&Timer, TIMER_1MICROSECOND*lto );
	   Count++;
	   if(Count == 5){
		   LengthInPage =  *(data + 3) + (*(data + 4)<<8);//LEN_OFFSET);
		   xil_printf("expected pages: %d or 0x%x\n\r",LengthInPage,LengthInPage);
		   Size = LengthInPage * 256;
		   xil_printf("expected bytes: %d or 0x%x\n\r",Size,Size);

		   OffsetInSector =  *(data + 1) + (*(data + 2)<<8);
		   xil_printf("offset sector: %d or 0x%x\n\r",OffsetInSector,OffsetInSector);

	   }

	   //Process header, to get size
	  // if (Count == MSG_HEADER_SIZE ) { 	//a page is collected. Now process the page
		   //ProcessHeader();
	  // }

	   //A message is done
	   if (Count == Size ) { 	//a page is collected. Now process the page
			   print("End of UploadFile, get out of cycle\n");
			   XScuTimer_Stop(&Timer);
			   XScuTimer_ClearInterruptStatus(&Timer);
			   ClearTimerFlag();

		   break;
	   }
    }

    xil_printf("received bytes: %d\n\r",Count);
	if(WriteFlashEx(data + PACKET_OVERHEAD ,OffsetInSector, LengthInPage)){ // start sector 0 for now
		xil_printf("Written page: %d\n\r",LengthInPage);
	}
}

/***
 *
* The function writes to the  serial FLASH connected to the QSPI interface.
* All the data put into the buffer must be in the same page of the device with
* page boundaries being on 256 byte boundaries.
*
* @param	buffer contains the address to write data to in the FLASH.
* @param	OffsetSec contains the start sector number(in sector).
* @param	LengthInPage is the length of the data in page(256 bytes)
*
* @return	1.
*
* @note		None
 *
 *
 */
extern u8 WriteBuffer[];
int WriteFlashEx(u8* Buffer,u32 OffsetSec,u32 LengthInPage){
	u32 OffsetInByte;

	u32 LengthInByte = LengthInPage * PAGE_SIZE; //Total
	u32 SecStartAddr =  OffsetSec * SECTOR_SIZE;

	FlashErase(&QspiInstance, SecStartAddr , LengthInByte);

	for(u16 i=0; i<LengthInPage; i++){
		OffsetInByte = i * PAGE_SIZE;
		memcpy(WriteBuffer+DATA_OFFSET,Buffer + OffsetInByte ,PAGE_SIZE);
		//Write to flash
		FlashWrite(&QspiInstance, SecStartAddr + OffsetInByte, PAGE_SIZE, WRITE_CMD);
		xil_printf("pw: %d/%d\n",i,LengthInPage);
	}
	return 1;
}

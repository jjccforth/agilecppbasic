/*
 * data_transfer.h
 *
 *  Created on: 2015��2��3��
 *      Author: jchen
 */

#ifndef DATA_TRANSFER_H_
#define DATA_TRANSFER_H_
#ifdef __cplusplus
extern "C" {
#endif

/*********Timer interrupt related constant and functions************/
#define UART_READBYTE_FLAG 0x0001
void SetTimerFlag(u32 mask);
void ClearTimerFlag();

u8  XUartPs_RecvByteEx(u32 BaseAddress, u32 timeout , char * result);



#ifdef __cplusplus
}
#endif
#endif /* DATA_TRANSFER_H_ */

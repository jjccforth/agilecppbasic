/*
 * data_transfer.c
 *
 *  Created on: 2015��2��3��
 *      Author: jchen
 */
#include <stdlib.h>
#include <unistd.h>
#include "xuartps_hw.h"
#include "data_transfer.h"
#include "xscutimer.h"


//333,333,333 ticks, i.e. 1s
//#define TIMER_LOAD_VALUE	0x13DE4355
//333 ticks i.e. 1 microsecond
#define TIMER_1MICROSECOND	0x14D

extern XScuTimer Timer;
extern u32  TimerFlag;
/****************************************************************************/
/**
*
* This function is an extension version of XUartPs_RecvByte which was block mode receiving.
*
* @param	BaseAddress contains the base address of the device.
*			timeout contains the microsecond for timeout value
*			result	contains a pointer that accept the result value. 0 for success, -1 for timeout
* @return	The data byte received.
*
* @note		None.
*
*****************************************************************************/
u8 XUartPs_RecvByteEx(u32 BaseAddress, u32 timeout , char * result)
{

		/*
		 * Wait until there is data ,or TimerFlag is cleared
		 */
		while (!XUartPs_IsReceiveData(BaseAddress)){
			if(TimerFlag == 0) {// If TimerFlag is cleared by timer ISR, get out of
				if( result != NULL)
					* result = -1;
				return 0;
			}
			//else continue waiting for data
		}
		/*
		 * Return the byte received
		 */
		if( result != NULL)
			* result = 0;
		return (XUartPs_ReadReg(BaseAddress, XUARTPS_FIFO_OFFSET));
}

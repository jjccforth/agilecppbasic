This is a basic extensible command line tools for a bare metal system as a tool for testing.It needs common lib functions to do the real job.

Currently it works as an command interpreter running on MicroZed board, waiting for incoming command from UART, and execute command accordingly.

Here are implemented functions and correspondent command:
*Read flash pages =>DumpFlash(DF)
*Write data to flash =>RecieveFileEx(UFX)
*Erase flash=>EraseFlashCmd(EF)
*GPIO output=>ToggleLed(BL),BlinkFewTimes(BLMORE)
*GPIO input=>ButtonPress,LongPress(LP)
And time interruption

Any new function can be added into the command system so it can be tested with an interactive way.

Note:
* Before flash writing, the content should be erased first. Erase will be effective on a sector (64K bytes)
* Flash writing should align with the boundary of 256 bytes(Page).
